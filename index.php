<?php

$f3=require_once('lib/base.php');

// Load configuration
$f3->config('config.ini');

ini_set("memory_limit",'1002M');
//require_once('generate.php');

$f3->set('DEBUG',2);
$f3->set('UI','ui/');

if($_SERVER["HTTP_HOST"] != 'localhost:8888'){
ini_set('include_path','/Users/Sebastian/cfscert');

$f3->set('DB',
  new DB\SQL(
    'mysql:host=localhost;port=3306;dbname=cfscert',
    'sqluser',
    'DQS123DQS!'
  )
);
}else{
$f3->set('DB',
  new DB\SQL(
    'mysql:host=localhost;port=3306;dbname=cfscert',
    'root',
    'root'
  )
);
}

$f3->set('menu',array(
        array(

            'label' => 'Start',
            'path' => 'start' ,
            'icon' => 'plus' ),
            array(

                'label' => 'Start & Reset',
                'path' => 'start/reset' ,
                'icon' => 'remove-circle' ),
));

$f3->set('admin_menu',array(


        array(
            'label' => 'Zertifikatstypen',
            'path' => 'admin/certtypes' ,
            'icon' => 'certificate' ),
        array(
          'label' => 'Text Elemente',
          'path' => 'admin/textelements' ,
          'icon' => 'edit'),
        array(
            'label' => 'Bildplatzhalter',
            'path' => 'admin/imgelements' ,
            'icon' => 'edit'),
          array(
            'label' => 'Nutzer',
            'path' => 'admin/user' ,
            'icon' => 'user'),
));




$f3->route('GET /start/filterlist/@current/@query','filterCertList');
$f3->route('GET /start/filterlist/@current','filterCertList');
$f3->route('GET /start/filterlist/all/@query','filterCertList');
$f3->route('GET /start/filterlist/all','filterCertList');

function filterCertList($f3){

  $f3->set('idtopass',$f3->get('PARAMS.current'));

  if(null !== $f3->get('PARAMS.current')){
    $but=" AND j.gclabel = 0 AND j.active = 1 AND j.ident<>".$f3->get('PARAMS.current');
  }else{
    $but=" AND j.gclabel = 0 AND j.active = 1";
  }


  $query = filter_var($f3->get('PARAMS.query'), FILTER_SANITIZE_STRING);
  $db=$f3->get('DB');

  $list = $db->exec("
      SELECT *, (SELECT COUNT(i.ident) FROM certificates i WHERE i.ident=j.ident) AS variations FROM certificates j
       WHERE (j.title LIKE '%$query%'$but) OR (j.ident LIKE '%$query%'$but) GROUP BY j.ident ORDER BY j.lang ASC
      ");

//SELECT elements.*, count(elements_id) AS noinstances FROM elements LEFT JOIN cert_elements ON (elements.id = cert_elements.elements_id) '.$searchstring.' GROUP BY elements.id ORDER BY y ASC, gclabel ASC

  $f3->set('list',$list);

  //display the admin view

  echo Template::instance()->render('new_cert_certlist.htm');
}


$f3->set('ONERROR',function($f3){
  $f3->set('html_title','Zertifikatsvorlage wählen');
  $f3->set('navpath','start');

  $f3->set('idtopass','');

  if(null == $f3->get('SESSION.error_msg')){
    $f3->set('SESSION.error_msg','Seite nicht gefunden.');
  }

  $f3->set('content','error.htm');

  echo Template::instance()->render('layout.htm');

  $f3->clear('SESSION.error_msg');
});

//home page
$f3->route('GET /start','newCert');
$f3->route('GET /start/@clear','newCert');
$f3->route('GET /','newCert');

  function newCert($f3) {
    $f3->set('html_title','Zertifikatsvorlage wählen');


    $f3->set('idtopass','');

    //check if reset of transferred data is requested
    if($f3->get('PARAMS.clear')){
      unsetTransferData($f3);
      $f3->set('navpath','start/reset');
    }else{
      $f3->set('navpath','start');
    }



    $cert=new DB\SQL\Mapper($f3->get('DB'),'certificates');
    $list=$cert->find('',array('order'=>'gclabel ASC'));
    $f3->set('list',$list);
    //display the admin view
    $f3->set('fillform','new_cert_howto.htm');
  //  $f3->set('pdffile','blank.htm');
    $f3->set('content','new_cert.htm');

    echo Template::instance()->render('layout.htm');
  }

  $f3->route('GET /certificates/success/@pdfname','successCert');


    function successCert($f3) {
      $f3->set('html_title','Zertifikatsvorlage wählen');
      $f3->set('navpath','start');

      $filename=$f3->get('PARAMS.pdfname');

      $f3->set('filename',$filename);

      $f3->set('path_pdf',$f3->get('BASE').'/tmp/'.$filename);
      $f3->set('path_pdf2img',pdf2image($filename,'/tmp/','H',true,$f3));

    //  $f3->set('content','new_cert_success.htm');
    //  $f3->set('pdffile','blank.htm');

    // if it is a label skip the download pdf/jpg sceen and open pdf instead
    if(strpos($filename, 'Label') == false){
      $f3->reroute('/tmp/'.$filename);
    }else{
      echo Template::instance()->render('new_cert_success.htm');
    }



    }


$f3->route('GET /certificates/download/img/@imgname','downloadCert');
$f3->route('GET /certificates/download/pdf/@pdfname','downloadCert');
function downloadCert($f3){
  if($f3->get('PARAMS.pdfname')){
    $filename=$f3->get('PARAMS.pdfname');
  }else{
    $filename=$f3->get('PARAMS.imgname');
  }
  echo $filename;
}

// Textelements
$f3->route('GET /certificates/fill/@cert_ident',
  function ($f3) {



    $f3->set('html_title','Neues Zertifikat ausstellen');
    $f3->set('navpath','start');

    $idtopass=$f3->get('PARAMS.cert_ident');
    $f3->set('idtopass',$idtopass); //poplate to template engine


    $f3->set('SESSION.generateid',$idtopass.time());
    $f3->clear('SESSION.generatelogo');


    //to put current cert Info on top of search field
    $cert=new DB\SQL\Mapper($f3->get('DB'),'certificates');
    if ($idtopass) $cert->load('gclabel = 0 AND ident='.$idtopass);
    $f3->set('item',$cert);

    //if empty result - trigger error
    if(null == $cert->id){
      $f3->set('SESSION.error_msg','Kein Zertifikat mit dieser ID gefunden');
      $f3->error(404);
    }

    $f3->set('generateform','');

    //get all certs with ident
    $connected=$f3->get('DB')->exec('SELECT * FROM certificates WHERE ident='.$idtopass.' ORDER BY gclabel ASC');
    //run through results and generate forms
    $i=1;
    $ajaxcopyinfo="";
    foreach($connected as $key=>$val){

      $f3->concat('generateform',generateCertForm($connected[$key]['id'],count($connected),$connected[$key]['pdftemplate'],$connected[$key]['title'],$connected[$key]['ident'],$connected[$key]['kva'],$connected[$key]['lang'],$f3));

      if(($i == 1) && (count($connected) > 1)){
        $f3->concat('generateform','<div class="col-md-1 certcopybutton"><button type="button" id="copy" class="btn btn-warning">
          <span class="glyphicon glyphicon-duplicate"></span> <span class="glyphicon glyphicon-arrow-right"></span>
          </button> </div>');}
        $formid[$i]=$connected[$key]['id'];
        $ajaxcopyinfo.="  $('[name=' + $(this).attr('name') + ']', '#certform-".$formid[$i]."').val($(this).val());";
        $i++;
      }

    //copy from one form to another
    if(count($connected)>1){$javascript="
<script>

    $('#copy').click(function () {
    $(':input[name]', '#certform-".$formid[1]."').each(function () {
    ".$ajaxcopyinfo."
    })
  });

    </script>";
    $f3->concat('generateform',$javascript);}




//foreach($var as $key=>$val){



    //$f3->set('previewimg',pdf2image($cert->pdftemplate,"/resources/pdf/",'L',$f3));

    $f3->set('currentCert','new_cert_current.htm');

    $f3->set('content','new_cert.htm');





    $f3->set('fillform','new_cert_form.htm');



    echo template::instance()->render('layout.htm');
  }
);


$f3->route('GET /login','loginForm');
$f3->route('GET /login/@fail','loginForm');
  function loginForm ($f3) {

    $f3->set('html_title','Adminlogin');
    $f3->set('navpath','login');


    $f3->set('content','loginform.htm');
    echo template::instance()->render('layout.htm');
  }

function chkauth($pw) {
    return md5($pw);
}

function myEncode($string,$key){
  $iv = mcrypt_create_iv(
    mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
    MCRYPT_DEV_URANDOM
);

  return base64_encode(
    $iv .
    mcrypt_encrypt(
        MCRYPT_RIJNDAEL_128,
        hash('sha256', $key, true),
        $string,
        MCRYPT_MODE_CBC,
        $iv
    )
  );

}
function myDecode($string,$key){
  $data = base64_decode($string);
$iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

return rtrim(
    mcrypt_decrypt(
        MCRYPT_RIJNDAEL_128,
        hash('sha256', $key, true),
        substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
        MCRYPT_MODE_CBC,
        $iv
    ),
    "\0"
);

}

$f3->route('POST /login','loginUser');
function loginUser($f3) {

    $user=new DB\SQL\Mapper($f3->get('DB'),'user');
    $user->load(array('username=?',$f3->get('POST.username')));

    if($user->password == md5($f3->get('POST.password'))) {


        $f3->set('SESSION.user',myEncode($f3->get('POST.username'),$f3->get('APPKEY')));

        $f3->set('SESSION.lastseen',time());

        $f3->reroute('/admin/user');
    }else{
      $f3->reroute('/login/loginfail');
    }

}

function rerouteNonAdmin($f3,$sessionusername) {

    $sessionusername=myDecode($sessionusername,$f3->get('APPKEY'));

    $user=new DB\SQL\Mapper($f3->get('DB'),'user');
    $user->load(array('username=?',$sessionusername));

    if($user->adminuser == false) {
        $f3->reroute('/admin/textelements');
    }
}

function rerouteNonLoggedin($f3,$sessionusername) {

  if(NULL != $sessionusername){

    $sessionusername=myDecode($sessionusername,$f3->get('APPKEY'));
    $user=new DB\SQL\Mapper($f3->get('DB'),'user');
    $user->load(array('username=?',$sessionusername));


    if(strcmp($user->username,$sessionusername) != 0) {
        $f3->reroute('/login/'.$sessionusername);
    }
  }else{
    $f3->reroute('/login/'.$sessionusername);
  }


}


$f3->route('GET /logout',
  function ($f3) {


  		$f3->clear('SESSION');


    $f3->reroute('/');
  }
);

// Admin Home
$f3->route('GET /admin/user',
  function ($f3) {
  //  rerouteNonLoggedin($f3,$f3->get('SESSION.user'));
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));
    rerouteNonAdmin($f3,$f3->get('SESSION.user'));

    $f3->set('html_title','Nutzerverwaltung');
    $f3->set('navpath','admin/user');

    $cert=new DB\SQL\Mapper($f3->get('DB'),'user');
    $list=$cert->find();
    $f3->set('list',$list);
    //display the admin view
    $f3->set('content','user_list.htm');
    echo template::instance()->render('layout.htm');
  }
);

//Admin Add
$f3->route('GET /admin/user/add',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));
    rerouteNonAdmin($f3,$f3->get('SESSION.user'));

    $f3->set('html_title','Neuer Nutzer');
    $f3->set('navpath','admin/user');

//<option value="F" {{ @active?'selected="selected"':'' }}>Female</option>
    $f3->set('POST.adminuser',0);

    $f3->set('content','user_edit.htm');
    echo template::instance()->render('layout.htm');
  }
);


$f3->route('GET /admin/user/edit/@id','editUser');
$f3->route('GET /admin/user/edit/@id/@pwchange','editUser');
function editUser($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));
    rerouteNonAdmin($f3,$f3->get('SESSION.user'));

    $f3->set('html_title','Nutzer bearbeiten');
    $f3->set('navpath','admin/user');

    $id = $f3->get('PARAMS.id');
    $f3->set('pwchange',$f3->get('PARAMS.pwchange'));


    $cert=new DB\SQL\Mapper($f3->get('DB'),'user');
    $cert->load(array('id=?',$id));
    $cert->copyTo('POST');



    $f3->set('content','user_edit.htm');
    echo template::instance()->render('layout.htm');
  }



$f3->route('POST /admin/user/edit/@id','AddEditUser');
$f3->route('POST /admin/user/edit','AddEditUser');
function AddEditUser($f3) {
  rerouteNonLoggedin($f3,$f3->get('SESSION.user'));
  rerouteNonAdmin($f3,$f3->get('SESSION.user'));

  $id = $f3->get('PARAMS.id');
  //create an article object
  $user=new DB\SQL\Mapper($f3->get('DB'),'user');
  //if we don't load it first Mapper will do an insert instead of update when we use save command
  if ($id) $user->load(array('id=?',$id));
  //overwrite with values just submitted

  // check if Checkbox is selected - if not set val to 0
  if (null != $f3->get('POST.password')) {
    $f3->set('POST.password',md5($f3->get('POST.password')));
  }

  // check if Checkbox is selected - if not set val to 0
  if (null == $f3->get('POST.adminuser')) {
    $f3->set('POST.adminuser','0');
  }

  $user->copyFrom('POST');

  $user->save();
  // Return to admin home page, new blog entry should now be there
  $f3->reroute('/admin/user');
}

$f3->route('GET /admin/user/delete/@id',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    $id = $f3->get('PARAMS.id');
    $article=new DB\SQL\Mapper($f3->get('DB'),'user');
    $article->load(array('id=?',$id));
    $article->erase();


    $f3->reroute('/admin/user');
  }
);


// Admin Home
$f3->route('GET /admin/certtypes',
  function ($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));


    $f3->set('html_title','Zertifikatstypen');
    $f3->set('navpath','admin/certtypes');

    $cert=new DB\SQL\Mapper($f3->get('DB'),'certificates');
    $list=$cert->find('',array('order'=>'ident ASC, gclabel ASC'));
    $f3->set('list',$list);
    //display the admin view
    $f3->set('content','cert_list.htm');
    echo template::instance()->render('layout.htm');
  }
);

//Admin Add
$f3->route('GET /admin/certtypes/add',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));
    //if (!$f3->get('SESSION.user')) $f3->error(401);
    $f3->set('html_title','Neuer Zertifikatstyp');
    $f3->set('navpath','admin/certtypes');


      //set defaults
      $f3->set('POST.gclabel','0');
      $f3->set('POST.active','0');

    $f3->set('content','cert_edit.htm');
    echo template::instance()->render('layout.htm');
  }
);

//Admin Add
$f3->route('GET /admin/imgelements',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));
    //if (!$f3->get('SESSION.user')) $f3->error(401);
    $f3->set('html_title','Bildplatzhalter');
    $f3->set('navpath','admin/imgelements');


      //set defaults
      $f3->set('POST.gclabel','0');
      $f3->set('POST.active','0');

      $dir_images    = 'resources/image';
      $dir_signatures    = 'resources/signatures';

      $f3->set('list_images',dirToArray($dir_images));
      $f3->set('list_signatures',dirToArray($dir_signatures));


    $f3->set('content','imgelements_edit.htm');
    echo template::instance()->render('layout.htm');
  }
);

function dirToArray($dir) {

   $result = array();

   $cdir = scandir($dir);
   foreach ($cdir as $key => $value)
   {
      if (!in_array($value,array(".","..",".DS_Store")))
      {
         if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
         {
            $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
         }
         else
         {
            $result[] = $value;
         }
      }
   }

   return $result;
}

//Admin Edit
$f3->route('GET /admin/certtypes/edit/@id',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    $f3->set('html_title','Zertifikatstyp bearbeiten');
    $f3->set('navpath','admin/certtypes');

    $id = $f3->get('PARAMS.id');
    $cert=new DB\SQL\Mapper($f3->get('DB'),'certificates');
    $cert->load(array('id=?',$id));


    // ERROR HANDLING

    if(null == $cert->id){
      $f3->set('SESSION.error_msg','Kein Zertifikat mit dieser ID gefunden');
      $f3->error(404);
    }
    // END ERROR HANDLING


    $cert->copyTo('POST');
    $f3->set('content','cert_edit.htm');
    echo template::instance()->render('layout.htm');
  }
);


//Admin Delete
$f3->route('GET /admin/certtypes/delete/@id',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    $id = $f3->get('PARAMS.id');
    $article=new DB\SQL\Mapper($f3->get('DB'),'certificates');
    $article->load(array('id=?',$id));
    $article->erase();

    $f3->get('DB')->exec('DELETE FROM cert_elements WHERE certificates_id='.$f3->get('PARAMS.id'));

    $f3->reroute('/admin/certtypes');
  }
);

//Activate
$f3->route('GET /admin/certtypes/activate/@id',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    $id = $f3->get('PARAMS.id');


    $f3->get('DB')->exec('UPDATE certificates SET active=1 WHERE id='.$f3->get('PARAMS.id'));

    $f3->reroute('/admin/certtypes');
  }
);
//Deactive
$f3->route('GET /admin/certtypes/deactivate/@id',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    $id = $f3->get('PARAMS.id');


    $f3->get('DB')->exec('UPDATE certificates SET active=0 WHERE id='.$f3->get('PARAMS.id'));

    $f3->reroute('/admin/certtypes');
  }
);




$f3->route('GET /admin/textelements/filter/@column/@value','getTextElements');
$f3->route('GET /admin/textelements/filter/@column','getTextElements');
$f3->route('GET /admin/textelements/filter','getTextElements');
$f3->route('GET /admin/textelements','getTextElements');

  function getTextElements($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    $f3->set('html_title','Text Elemente');
    $f3->set('navpath','admin/textelements');

    $col = $f3->get('PARAMS.column');
    $val = $f3->get('PARAMS.value');


    //check if filter is set in Session and reroute
    //if not - set session vars based on filterlist
    //if "all" - reset session vars
    if((strlen($f3->get('PARAMS.column')) > 0) && (strlen($f3->get('PARAMS.column')) > 0)){

      if($f3->get('PARAMS.column') == "all"){
          $f3->clear("SESSION.filtercol");
          $f3->clear("SESSION.filterval");
          $f3->reroute('/admin/textelements');
        }else{
      if($f3->get("SESSION.filtercol") != $col){$f3->set("SESSION.filtercol",$col);}
      if($f3->get("SESSION.filterval") != $col){$f3->set("SESSION.filterval",$val);}
        }
    }else{
      if((strlen($f3->get("SESSION.filterval")) > 0) && (strlen($f3->get("SESSION.filtercol")) > 0)){
        $f3->reroute('/admin/textelements/filter/'.$f3->get("SESSION.filtercol").'/'.$f3->get("SESSION.filterval"));
      }
    }


    if(isset($col) && isset($val)){
    $searchstring="WHERE ".$col." = '".$val."'";
    }else{
      $searchstring="";
    }

    //$cert=new DB\SQL\Mapper($f3->get('DB'),'elements');
    //$list=$cert->find($searchstring,array('order'=>'y ASC, gclabel ASC'));

    $list=$f3->get('DB')->exec('SELECT elements.*, count(elements_id) AS noinstances FROM elements LEFT JOIN cert_elements ON (elements.id = cert_elements.elements_id) '.$searchstring.' GROUP BY elements.id ORDER BY y ASC, gclabel ASC');
    $f3->set('list',$list);

    //if empty result - trigger error
    if(count($f3->get('list')) < 1){
      $f3->set('SESSION.error_msg','Keine Elemente mit diesem Filter gefunden');
      $f3->set('SESSION.error_elementfilter','on');
      $f3->error(404);
    }

    $f3->set("certidents",$f3->get('DB')->exec("SELECT c.id,c.ident,c.title FROM certificates c JOIN elements e WHERE c.ident=e.cert_ident GROUP BY ident"));

  //  $f3->set('list',$list);
    //display the admin view
    $f3->set('content','element_list.htm');
    echo template::instance()->render('layout.htm');
  }

$f3->route('POST /admin/textelements',
  function ($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));
    $f3->set('html_title','Text Elemente');
    $f3->set('navpath','admin/textelements');

    $string = $f3->get('POST.typeahead');

    //$cert=new DB\SQL\Mapper($f3->get('DB'),'elements');

    //$list=$cert->find('text='.$string,array('order'=>'y ASC'));

    //$list=$f3->get('DB')->exec('SELECT * FROM elements WHERE text LIKE "%'.$string.'%"');
    $list=$f3->get('DB')->exec('SELECT elements.*, count(elements_id) AS noinstances FROM elements LEFT JOIN cert_elements ON (elements.id = cert_elements.elements_id) WHERE text LIKE "%'.$string.'%" GROUP BY elements.id ORDER BY y ASC, gclabel ASC');

    $f3->set("certidents",$f3->get('DB')->exec("SELECT id,ident,title FROM certificates GROUP BY ident"));

    $f3->set('list',$list);
    //display the admin view
    $f3->set('content','element_list.htm');
    echo template::instance()->render('layout.htm');
  }
);


//Admin Add
$f3->route('GET /admin/textelements/add',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    $f3->set('html_title','Text Elemente Zertifikatstyp');
    $f3->set('navpath','admin/textelements');

    // Set standard vars so template renders without errror
    $f3->set('POST.isvar','0');
    $f3->set('POST.print','1');
    $f3->set('POST.gclabel','0');
    $f3->set('POST.cert_ident','0');
    $f3->set('POST.type','text');
    $f3->set('POST.lang','EN');


    $f3->set("certidents",$f3->get('DB')->exec("SELECT id,ident,title FROM certificates GROUP BY ident"));

    $f3->set('content','element_edit.htm');
    echo template::instance()->render('layout.htm');
  }
);

//Admin Edit
$f3->route('GET /admin/textelements/edit/@id',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    $f3->set('html_title','Text Elemente bearbeiten');
    $f3->set('navpath','admin/textelements');

    $id = $f3->get('PARAMS.id');
    $cert=new DB\SQL\Mapper($f3->get('DB'),'elements');
    $cert->load(array('id=?',$id));
    $cert->copyTo('POST');

    //if empty result - trigger error
    if(null ==$cert->id){
      $f3->set('SESSION.error_msg','Kein Textelement mit dieser ID gefunden');
      $f3->error(404);
    }

    $f3->set("certidents",$f3->get('DB')->exec("SELECT id,ident,title FROM certificates GROUP BY ident"));



    $f3->set('content','element_edit.htm');
    echo template::instance()->render('layout.htm');
  }
);

$f3->route('POST /admin/textelements/edit/@id','editTextElements');
$f3->route('POST /admin/textelements/add','editTextElements');
function editTextElements($f3) {
  rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

  $id = $f3->get('PARAMS.id');
  //create an article object
  $cert=new DB\SQL\Mapper($f3->get('DB'),'elements');
  //if we don't load it first Mapper will do an insert instead of update when we use save command
  if ($id) $cert->load(array('id=?',$id));
  //overwrite with values just submitted

  // check if Checkbox is selected - if not set val to 0
  if (null == $f3->get('POST.isvar')) {
    $f3->set('POST.isvar','0');
  }
  if (null == $f3->get('POST.print')) {
    $f3->set('POST.print','0');
  }
  if (null == $f3->get('POST.gclabel')) {
    $f3->set('POST.gclabel','0');
  }

  $cert->copyFrom('POST');



  $cert->save();
  // Return to admin home page, new blog entry should now be there
  $f3->reroute('/admin/textelements');
}

$f3->route('GET /admin/textelements/delete/@id',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    // ERROR HANDLING
    $id = $f3->get('PARAMS.id');
    $tmpquery=new DB\SQL\Mapper($f3->get('DB'),'elements');
    $tmpquery->load(array('id=?',$id));

    if(null == $tmpquery->id){
      $f3->set('SESSION.error_msg','Kein Textelement mit dieser ID gefunden');
      $f3->error(404);
    }
    // END ERROR HANDLING

    $tmpquery->erase();

    $f3->get('DB')->exec('DELETE FROM cert_elements WHERE elements_id='.$f3->get('PARAMS.id'));

    $f3->reroute('/admin/textelements');
  }
);

$f3->route('GET /admin/textelements/copy/@id',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    // ERROR HANDLING
    $id = $f3->get('PARAMS.id');
    $tmpquery=new DB\SQL\Mapper($f3->get('DB'),'elements');
    $tmpquery->load(array('id=?',$id));

    if(null == $tmpquery->id){
      $f3->set('SESSION.error_msg','Kein Textelement mit dieser ID gefunden - kann nicht kopiert werden');
      $f3->error(404);
    }
    // END ERROR HANDLING


    $f3->get('DB')->exec('CREATE TEMPORARY TABLE tmp_textelement_'.$f3->get('PARAMS.id').' SELECT * FROM elements WHERE id='.$f3->get('PARAMS.id'));
    $f3->get('DB')->exec('UPDATE tmp_textelement_'.$f3->get('PARAMS.id').' SET id=NULL WHERE id='.$f3->get('PARAMS.id'));
    $f3->get('DB')->exec('INSERT INTO elements SELECT * FROM tmp_textelement_'.$f3->get('PARAMS.id'));
    $f3->get('DB')->exec('DROP TEMPORARY TABLE tmp_textelement_'.$f3->get('PARAMS.id'));

    $f3->reroute('/admin/textelements');
  }
);

$f3->route('GET /admin/certtypes/copy/@id',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    // ERROR HANDLING
    $id = $f3->get('PARAMS.id');
    $tmpquery=new DB\SQL\Mapper($f3->get('DB'),'certificates');
    $tmpquery->load(array('id=?',$id));

    if(null == $tmpquery->id){
      $f3->set('SESSION.error_msg','Kein Zertifikat mit dieser ID gefunden - kann nicht kopiert werden');
      $f3->error(404);
    }
    // END ERROR HANDLING

    $f3->get('DB')->exec('CREATE TEMPORARY TABLE tmp_certificates_'.$f3->get('PARAMS.id').' SELECT * FROM certificates WHERE id='.$f3->get('PARAMS.id'));
    $f3->get('DB')->exec('UPDATE tmp_certificates_'.$f3->get('PARAMS.id').' SET id=NULL, active=0 WHERE id='.$f3->get('PARAMS.id'));
    $f3->get('DB')->exec('INSERT INTO certificates SELECT * FROM tmp_certificates_'.$f3->get('PARAMS.id'));
    $id = $f3->get('DB')->lastInsertId();
    $f3->get('DB')->exec('DROP TEMPORARY TABLE tmp_certificates_'.$f3->get('PARAMS.id'));

    $f3->get('DB')->exec('CREATE TEMPORARY TABLE tmp_cert_elements_'.$f3->get('PARAMS.id').' SELECT * FROM cert_elements WHERE certificates_id='.$f3->get('PARAMS.id'));
    $f3->get('DB')->exec('UPDATE tmp_cert_elements_'.$f3->get('PARAMS.id').' SET certificates_id='.$id.' , id=NULL WHERE certificates_id='.$f3->get('PARAMS.id'));
    $f3->get('DB')->exec('INSERT INTO cert_elements SELECT * FROM tmp_cert_elements_'.$f3->get('PARAMS.id'));
    $f3->get('DB')->exec('DROP TEMPORARY TABLE tmp_cert_elements_'.$f3->get('PARAMS.id'));

    $f3->reroute('/admin/certtypes');
  }
);

$f3->route('GET /admin/certtypes/design/@id',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    $f3->set('html_title','Zertifikatstyp gestalten');
    $f3->set('navpath','admin/certtypes');

    $id = $f3->get('PARAMS.id');

    // ERROR HANDLING
    $cert=new DB\SQL\Mapper($f3->get('DB'),'certificates');
    $cert->load(array('id=?',$id));
    $cert->copyTo('item');

    if(null == $cert->id){
      $f3->set('SESSION.error_msg','Kein Zertifikat mit dieser ID gefunden - kann designed werden');
      $f3->error(404);
    }
    // END ERROR HANDLING


    if($cert->gclabel == true){
      $addargs=" gclabel=1";
    }else{
      $addargs=" gclabel=0";
    }

    // get all elements currently in use
    $f3->set('usedelements',$f3->get('DB')->exec('SELECT * FROM elements a JOIN cert_elements b ON a.id = b.elements_id WHERE b.certificates_id='.$f3->get('PARAMS.id').' ORDER BY a.y'));

    $f3->set('used_ids',$f3->get('DB')->exec('SELECT elements_id FROM elements a JOIN cert_elements b ON a.id = b.elements_id WHERE b.certificates_id='.$f3->get('PARAMS.id').' ORDER BY a.y'));

    $var=$f3->get('used_ids');

    if(count($var) > 0){
        $f3->set('args','(cert_ident = '.$cert->ident.' OR cert_ident = 0) AND (lang = "'.$cert->lang.'" OR lang = "XX") AND ');
    }else{
        $f3->set('args','(cert_ident = '.$cert->ident.' OR cert_ident = 0) AND (lang = "'.$cert->lang.'" OR lang = "XX")');
    }


    foreach($f3->get('used_ids') as &$val){
      if(is_array($val)){
        foreach ($val as $realval){
          $f3->concat('args',"id!=".$realval." AND ");
        }
      }
    }



  //  $f3->set('args',rtrim($f3->get('args'),"AND "));
    $f3->concat('args',$addargs);
    // get all elements not in use

    $elements=new DB\SQL\Mapper($f3->get('DB'),'elements');
    $list=$elements->find($f3->get('args'));
    $f3->set('elementslist',$list);


    $f3->set('content','cert_design.htm');


    echo template::instance()->render('layout.htm');
  }
);

$f3->route('GET /admin/textelements/removefromcert/@cert_id/@element_id',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    $f3->get('DB')->exec('DELETE FROM cert_elements WHERE certificates_id='.$f3->get('PARAMS.cert_id').' AND elements_id='.$f3->get('PARAMS.element_id'));
    $f3->reroute('/admin/certtypes/design/'.$f3->get('PARAMS.cert_id'));
  }
);
$f3->route('GET /admin/textelements/addtocert/@cert_id/@element_id',
  function($f3) {
    rerouteNonLoggedin($f3,$f3->get('SESSION.user'));

    $element_cert=new DB\SQL\Mapper($f3->get('DB'),'cert_elements');
    $element_cert->certificates_id=$f3->get('PARAMS.cert_id');
    $element_cert->elements_id=$f3->get('PARAMS.element_id');
    $element_cert->save();

//    $f3->get('DB')->exec('DELETE FROM cert_elements WHERE certificates_id='.$f3->get('PARAMS.cert_id').' AND elements_id='.$f3->get('PARAMS.cert_id'));
    $f3->reroute('/admin/certtypes/design/'.$f3->get('PARAMS.cert_id'));
  }
);


//Admin Add and Edit both deal with Form Posts
//don't use a lambda function here because there are 2 routes
$f3->route('POST /admin/certtypes/edit/@id','editCerttype');
$f3->route('POST /admin/certtypes/add','editCerttype');
function editCerttype($f3) {
  rerouteNonLoggedin($f3,$f3->get('SESSION.user'));


  $id = $f3->get('PARAMS.id');
  //create an article object
  $cert=new DB\SQL\Mapper($f3->get('DB'),'certificates');
  //if we don't load it first Mapper will do an insert instead of update when we use save command
  if ($id) $cert->load(array('id=?',$id));
  //overwrite with values just submitted
  $cert->copyFrom('POST');



  $cert->save();
  // Return to admin home page, new blog entry should now be there
  $f3->reroute('/admin/certtypes');
}

$f3->route('POST /certificates/generate/@id','generateCertContent');
$f3->route('POST /certificates/generate/','generateCertContent');
function generateCertContent($f3){

  $cert=new DB\SQL\Mapper($f3->get('DB'),'certificates');
  $cert->load(array('id=?',$f3->get('PARAMS.id')));


  $f3->set('usedelements',$f3->get('DB')->exec('SELECT * FROM elements a JOIN cert_elements b ON a.id = b.elements_id WHERE b.certificates_id='.$f3->get('PARAMS.id').' ORDER BY a.y ASC'));

  $var=$f3->get('usedelements');

  $postvar=$f3->get('POST');

  $certreg="";
  $certvalid="";
  $certdate="";

  foreach($var as $key=>$val){
    if(is_array($val)){
      foreach ($val as $realkey=>&$realval){

          foreach($postvar as $postkey=>$postval){
            if(!empty($realval)){
              if($realval==$postkey){
              //  echo $realval." wird ".$postval;
                if($var[$key]['text'] == "CERTREGNO"){$certreg=$postval;}
                if($var[$key]['text'] == "CertDate"){$certdate=$postval;}
                if($var[$key]['text'] == "CertValidFrom"){$certdate=$postval;}
                if($var[$key]['text'] == "CertValidUntil"){$certvalid=$postval;}

                $var[$key]['text'] = $postval;

                }




            }
        }

          // echo " <b>".$realkey."</b> -  <i>(".$realval.")</i><br>";

      } //echo "<br><br>";
    }
  }

  //echo $cert->kva;

  //replace QR placeholder with AZ Number
  foreach($var as $key=>$val){

    if($var[$key]['ident'] == "CERTREGNO"){$var[$key]['text'].=" ".$cert->kva;}

    if(is_array($val)){
      foreach ($val as $realkey=>&$realval){

          //$var[$key][$realkey] = str_replace("{CERTREGNO}",$certreg,$var[$key][$realkey]);
          $var[$key][$realkey] = str_replace("{GCNAME}",$cert->title,$var[$key][$realkey]);

          preg_match_all ("/\\{.*?\\}/",$var[$key]["text"],$treffer);
          if( count ($treffer[0])>0){

                for($i=0;$i<count($treffer[0]);$i++){

                //echo $i."<br>";
                        $searchkey=extractStringBetween("{","}",$treffer[0][$i]);
                        if(isset($postvar[$searchkey])){
                        $var[$key][$realkey] = str_replace($treffer[0][$i],$postvar[$searchkey],$var[$key][$realkey]);
                        }
                        //preg_match("/\\{.*?\\}\\s*/s",$v,$matches);

                      /*  if(isset($matches[1])){$searchkey=$matches[1];
                          echo "test";

                        }*/
                      //  $searchkey=extractStringBetween("{","}",$treffer[0]);


                      }
            }
          //  $var[$key][$realkey] = str_replace($treffer[0],$postvar[$searchkey],$var[$key][$realkey]);

            //echo extractStringBetween("{","}",$treffer[0]);


           //echo " <b>".$realkey."</b> -  <i>(".$realval.")</i><br>";

      } //echo "<br><br>";
    }
  }
  //echo "REG No is: ".$certreg;

  $nameaddon = ($cert->gclabel == 1 ? "Label":"");
  $printtype = ($cert->gclabel == 1 ? "GC":"Cert");

  switch ($cert->lang) {
    case 'EN':
      $lang='Englisch';
    break;
    case 'DE':
      $lang='Deutsch';
      break;
    case 'FR':
      $lang='Französisch';
    break;
    case 'RUS':
      $lang='Russisch';
    break;
    default:
      $lang='Englisch';
      break;
  }





  //  echo json_encode($var);
  //$filename=$certreg." - ".$cert->kva." ".$certvalid." - ".$cert->title." - ".$nameaddon." ".$cert->lang;
  $filename=$certreg." ".$cert->kva." ".$certdate." ".$lang." - ".$nameaddon;

  //echo var_dump($postvar["City"]);
  printCertificate($f3,'F','/tmp/',json_encode($var),getGCSampleImage($f3,$cert->ident),$filename,"de",$printtype);

}



function extractStringBetween($cFirstChar, $cSecondChar, $sString)
{
    preg_match("/\\".$cFirstChar."(.*?)\\".$cSecondChar."/", $sString, $aMatches);
    return $aMatches[1];
}

function getGCSampleImage($f3,$ident){
  $cert=new DB\SQL\Mapper($f3->get('DB'),'certificates');
  $cert->load(array('ident=? AND gclabel=?',$ident,1));
  return $cert->pdftemplate;
}

function definePDFcontent($f3, $json){

//echo $json;

$workvar = json_decode($json,true);
var_dump ($workvar);

foreach($workvar as $topkey=>$topval){
  echo $workvar[$topkey]['text']."<br>";
  foreach($topval as $realkey=>$realval){




  }
}



}

function generateCertForm($cert_id,$formcount,$pdftemplate,$certitle,$gcident,$kva,$lang,$f3){

  $f3->set('form','');



  if($formcount == 1){
    $preeach='<div class="col-md-12">';
    $appendeach='</div>';
  }elseif($formcount == 2){
    $preeach='<div class="col-md-5">';
    $appendeach='</div>';
  }elseif($formcount == 3){
    $preeach='<div class="col-md-3">';
    $appendeach='</div>';
  }elseif($formcount == 4){
    $preeach='<div class="col-md-2">';
    $appendeach='</div>';
  }
  else{
    $preeach='<div class="col-md-1">';
    $appendeach='</div>';
  }

  $f3->concat('form',$preeach);


  $f3->set('variables',$f3->get('DB')->exec('SELECT * FROM elements a JOIN cert_elements b ON a.id = b.elements_id WHERE a.isvar=true AND b.certificates_id='.$cert_id.' ORDER BY a.y'));

  $var=$f3->get('variables');
  //var_dump($var);

//$_SERVER['DOCUMENT_ROOT'].$f3->get('BASE').

  //$previewimg=pdf2image($pdftemplate,"/resources/pdf/",'L',$f3);
  $previewimg=pdf2image($pdftemplate,"/resources/pdf/",'M',false,$f3);

  $f3->concat('form','
  <div class="cert-infoheader">
    <div class="cert-infoheader-img">
    <img src="'.$previewimg.'" class="img-responsive img-thumbnail">
  </div>
  <div class="cert-infoheader-info">
  <h4>'.$certitle.' ('.$lang.')</h4>
  </div>
  <div class="clear"></div>
  </div>
  ');

  $f3->concat('form','<div class="form2fill"><form class="form-horizontal" id="certform-'.$cert_id.'" method="post" target="_blank" action="'.$f3->get('BASE').'/certificates/generate/'.$cert_id.'">');
  foreach($f3->get('variables') as &$val){
    if(is_array($val)){

      if(isset($_SESSION[$val['ident']])){
        $transferContent=$_SESSION[$val['ident']];
      }else{
        $transferContent="";
      }

      switch($val['type']){
        case "text":
        if($val['ident']=="GCIDENT"){
          $f3->concat('form','<div class="form-group"><label for="'.$val['ident'].'">'.$val['ident'].'</label>
                              <input type="text" class="form-control" id="'.$val['ident'].'" name="'.$val['ident'].'" placeholder="'.$gcident.'" value="'.$gcident.'" readonly>
                              </div><p></p>');
        }elseif($val['ident']=="CERTREGNO"){
        $f3->concat('form','<div class="form-group"><label for="'.$val['ident'].'">'.$val['ident'].'</label><div class="input-group">
                            <input type="text" class="form-control" id="'.$val['ident'].'" name="'.$val['ident'].'" placeholder="'.$val['text'].'" value="'.$transferContent.'">
                            <div class="input-group-addon">'.$kva.'</div>
                            </div></div><p></p>');
                          }else{
        $f3->concat('form','<div class="form-group"><label for="'.$val['ident'].'">'.$val['ident'].'</label>
                            <input type="text" class="form-control" id="'.$val['ident'].'" name="'.$val['ident'].'" placeholder="'.$val['text'].'" value="'.$transferContent.'">
                            </div><p></p>');
                          }
        break;
        case "textbox":
$f3->concat('form','<div class="form-group"><label for="'.$val['ident'].'">'.$val['ident'].'</label>
        <div class="input-group">
          <textarea cols="100" id="'.$val['ident'].'" name="'.$val['ident'].'" placeholder="'.$val['text'].'" class="form-control" rows="3">'.$transferContent.'</textarea>
          </div></div><p></p>');
        break;
        case "date":
        $f3->concat('form','<div class="form-group"><label for="'.$val['ident'].'">'.$val['ident'].'</label><div class="input-group date" data-provide="datepicker"><input type="text" class="form-control" id="'.$val['text'].'" name="'.$val['ident'].'" value="'.$transferContent.'"><div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span></div></div></div><p></p>');
        break;
      }


    }
  }


  $f3->concat('form','<center><button type="submit" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-cloud-download"></span></button></center>');
  $f3->concat('form','</form></div>');
  $f3->concat('form',$appendeach);


return $f3->get('form');
}



function pdf2image($pdf,$path,$quality="L",$force=false,$f3){

  $php_vs_arr = preg_split("/\./", phpversion());
           $php_vs = $php_vs_arr[0] . '.' . $php_vs_arr[1];

  if($f3->get('HOST') != 'localhost'){

      $im = new Imagick();

      if(($quality)== "H"){
        $im->setResolution(300,300);
      }elseif(($quality)== "M"){
        $im->setResolution(150,150);
      }else{
        $im->setResolution(73,73);
      }

      $file=$_SERVER['DOCUMENT_ROOT'].$f3->get('BASE').$path.$pdf;

      $imagepath=$_SERVER['DOCUMENT_ROOT'].$f3->get('BASE').$path.$pdf.'.jpg';


      if(file_exists($imagepath)){
        $lastchange=filemtime($imagepath);
      }else{
        $lastchange=(time() - (34 * 60 * 60));
      }

      $cachetime=(time() - (24 * 60 * 60));


      if(($lastchange < $cachetime) || ($quality == "H") || ($force !== false)){

        //die($lastchange." ".$cachetime." ".time());

        $im->readImage($file);



        $range = $im->getQuantumRange();

        if ($im->getImageColorspace() == \Imagick::COLORSPACE_CMYK) {
          //  $im->setImageColorspace(12);

      /*      $profiles = $im->getImageProfiles('*', false);
       // we're only interested if ICC profile(s) exist
       $has_icc_profile = (array_search('icc', $profiles) !== false);
       // if it doesnt have a CMYK ICC profile, we add one
       if ($has_icc_profile === false) {
           $icc_cmyk = file_get_contents($_SERVER['DOCUMENT_ROOT'].$f3->get('BASE').'resources/icc/WebCoatedFOGRA28.icc');
           $im->profileImage('icc', $icc_cmyk);
           unset($icc_cmyk);
       }*/
      // $im->setImageColorspace(12);



            /*$icc_rgb = file_get_contents($_SERVER['DOCUMENT_ROOT'].$f3->get('BASE').'resources/icc/AdobeRGB1998.icc');
            $im->profileImage('icc', $icc_rgb);
            unset($icc_rgb);*/

            $im->setImageColorspace(13);


            if($php_vs < 5.3) {
                    //ADJUST GAMMA BY 20% for 5.2.x
                    $im->levelImage(0, 2.0, $range['quantumRangeString']);
                } else {
                    //php 5.3 hack FOR INVERTED COLORS
                    $im->negateImage(false, Imagick::CHANNEL_ALL);
                }
        }


        if(($quality) == "H"){

        }elseif(($quality) == "M"){
          $im->scaleImage(500, 0);
        }else{
          $im->scaleImage(250, 0);
        }

        $im->setImageFormat('jpg');

        $im->writeImage($_SERVER['DOCUMENT_ROOT'].$f3->get('BASE').$path.$pdf.'.jpg');

        }

  }

  return $f3->get('BASE').$path.$pdf.'.jpg';

}

function rescaleImg($image,$wmax,$hmax) {
    // Scale Images and return new width and height

    list($width, $height, $type, $attr) = getimagesize($image);
    $hscale = $height / $hmax;
    $wscale = $width / $wmax;
    if (($hscale > 1) || ($wscale > 1)) {
        $scale = ($hscale > $wscale)?$hscale:$wscale;
    } else {
        $scale = 1;
    }
    $newwidth = floor($width / $scale);
    $newheight= floor($height / $scale);

    return array($newwidth,$newheight);
}

function printCertificate($f3,$where="I",$path="/tmp",$json="",$gcid="0",$filename,$ZipCityFormat="de",$printtype="GC",$lang="de"){


  $id = $f3->get('PARAMS.id');
  $cert=new DB\SQL\Mapper($f3->get('DB'),'certificates');
  $cert->load(array('id=?',$id));

//  require_once('./lib/fpdf/fpdf.php');
  require_once('./lib/fpdf/textbox.php');
require_once('./lib/tfpdf/tfpdf.php');
require_once('./lib/tcpdf/tcpdf.php');
  require_once('./lib/fpdi/fpdi.php');

  require_once('./lib/qr.php');



  $width=300;
  $height=300;
  $az=491767;

  $_tpl["tpl_img"]="./resources/img/gc-zert.jpg";
  $_tpl["tpl_pdf"]="./resources/pdf/".$cert->pdftemplate;
  $_tpl["url"]="https://www.mydqs.com/kunden/kundendatenbank.html?aoemydqs%5Bcompany_no%5D={$az}&aoemydqs%5Baction%5D=singleView";


  // Instanciation of inherited class
  // initiate FPDI
  $pdf = new FPDI();

  // add a page
  $pdf->AddPage();
  // set the source file
  $pdf->setSourceFile($_tpl["tpl_pdf"]);
  // import page 1
  $tplIdx = $pdf->importPage(1);
  // use the imported page and place it at position 10,10 with a width of 100 mm
  $pdf->useTemplate($tplIdx, 0, 0, 210);

  $pdf->SetTitle($cert->title,1);
  $pdf->SetAuthor('DQS Group');

  $pdf->SetMargins(50,20,15);
  $pdf->SetAutoPageBreak(0, 0);

  $pdf->SetTextColor($cert->cr,$cert->cg,$cert->cb);


  $workvar = json_decode($json,true);
//  var_dump ($workvar);
//  die(  var_dump ($workvar));

//precheck and preformat
foreach($workvar as $prekey=>$preval){
  if(($workvar[$prekey]['ident'] == "CompanyZipCode")){
    $ZipCode=$workvar[$prekey]['text'];
  }
  if(($workvar[$prekey]['ident'] == "CompanyCity")){
    $City=$workvar[$prekey]['text'];
  }
}
if(isset($ZipCode) AND isset($City)){
  $UseZipCity=1;
      if($ZipCityFormat == "de"){
      $ZipCity=$ZipCode." ".$City;
      }elseif($ZipCityFormat == "uk"){
      $ZipCity=$City." ".$ZipCode;
    }else{
      $UseZipCity=0;
      $ZipCity="ERROR - bitte an Admin wenden - Stichwort: ZipCity";
    }

}else{
    $UseZipCity=0;
    $ZipCity="ERROR - bitte an Admin wenden - Stichwort: ZipCity";
}

/*if($printtype != "GC"){
  $pdf->SetFont('helvetica', '', 12);
}else{
  $pdf->SetFont('dejavusans', '', 12);
}
*/
  $pdf->SetFont('arial', '', 12);

  //$pdf->SetFont('arialuni', $workvar[$topkey]['fontweight'], $workvar[$topkey]['fontsize']);


  foreach($workvar as $topkey=>$topval){

    //check what type of input comes our way and generate output elements

    if($workvar[$topkey]['print'] == false){

    }else{

        if($workvar[$topkey]['ident'] == "CompanyZipCode" OR $workvar[$topkey]['ident'] == "CompanyCity"){

              if($UseZipCity == 1){

              $pdf->SetXY($workvar[$topkey]['x'], $workvar[$topkey]['y']);

              switch($workvar[$topkey]['fontweight']){
                case '':
                $pdf->SetFont('arial', '', $workvar[$topkey]['fontsize']);
                break;
                case 'B':
                $pdf->SetFont('arialb', '', $workvar[$topkey]['fontsize']);
                break;
                case 'I':
                $pdf->SetFont('ariali', '', $workvar[$topkey]['fontsize']);
                break;
                case 'BI':
                $pdf->SetFont('arialbi', '', $workvar[$topkey]['fontsize']);
                break;
              }

            //  $pdf->MultiCell($workvar[$topkey]['width'],($workvar[$topkey]['fontsize']/2.5),$ZipCity,0,$workvar[$topkey]['align']);
                $pdf->MultiCell($workvar[$topkey]['width'], ($workvar[$topkey]['fontsize']/2.5), $ZipCity, 1, $workvar[$topkey]['align'], 1, 0, '', '', true);
            }else{

              $pdf->SetXY($workvar[$topkey]['x'], $workvar[$topkey]['y']);

              switch($workvar[$topkey]['fontweight']){
                case '':
                $pdf->SetFont('arial', '', $workvar[$topkey]['fontsize']);
                break;
                case 'B':
                $pdf->SetFont('arialb', '', $workvar[$topkey]['fontsize']);
                break;
                case 'I':
                $pdf->SetFont('ariali', '', $workvar[$topkey]['fontsize']);
                break;
                case 'BI':
                $pdf->SetFont('arialbi', '', $workvar[$topkey]['fontsize']);
                break;
              }

              //$pdf->MultiCell($workvar[$topkey]['width'],($workvar[$topkey]['fontsize']/2.5),$workvar[$topkey]['text'],0,$workvar[$topkey]['align']);
                $pdf->MultiCell($workvar[$topkey]['width'], ($workvar[$topkey]['fontsize']/2.5), $workvar[$topkey]['text'], 1, $workvar[$topkey]['align'], 1, 0, '', '', true);
            }

        }else{
                if(($workvar[$topkey]['type'] == "text") || ($workvar[$topkey]['type'] == "date")){




                $pdf->SetXY($workvar[$topkey]['x'], $workvar[$topkey]['y']);

                switch($workvar[$topkey]['fontweight']){
                  case '':
                  $pdf->SetFont('arial', '', $workvar[$topkey]['fontsize']);
                  break;
                  case 'B':
                  $pdf->SetFont('arialb', '', $workvar[$topkey]['fontsize']);
                  break;
                  case 'I':
                  $pdf->SetFont('ariali', '', $workvar[$topkey]['fontsize']);
                  break;
                  case 'BI':
                  $pdf->SetFont('arialbi', '', $workvar[$topkey]['fontsize']);
                  break;
                }



                $pdf->MultiCell($workvar[$topkey]['width'], ($workvar[$topkey]['fontsize']/2.5), $workvar[$topkey]['text'], 1, $workvar[$topkey]['align'], 1, 0, '', '', true);

              //  $pdf->MultiCell($workvar[$topkey]['width'],($workvar[$topkey]['fontsize']/2.5),$workvar[$topkey]['text'],0,$workvar[$topkey]['align']);
              }elseif($workvar[$topkey]['type'] == "qr"){
                $pdf->Image(generate_qr($workvar[$topkey]['text']),$workvar[$topkey]['x'],$workvar[$topkey]['y'],$workvar[$topkey]['width']);
              }elseif($workvar[$topkey]['type'] == "logo"){
                if($f3->get('SESSION.generatelogo')){

                  $dimensions=rescaleImg("./uploads/".$f3->get('SESSION.generatelogo'),$workvar[$topkey]['width'],($workvar[$topkey]['width']*0.6));

                  if($dimensions[0]<$workvar[$topkey]['width']){
                    $diff=$workvar[$topkey]['width']-$dimensions[0];
                    $workvar[$topkey]['x']=$workvar[$topkey]['x']+$diff;
                  }

                  $pdf->Image("./uploads/".$f3->get('SESSION.generatelogo'),$workvar[$topkey]['x'],$workvar[$topkey]['y'],$dimensions[0],$dimensions[1]);
                }
              }elseif($workvar[$topkey]['type'] == "signature"){

                  $pdf->Image("./resources/signatures/".$workvar[$topkey]['text'].".png",$workvar[$topkey]['x'],$workvar[$topkey]['y'],$workvar[$topkey]['width']);

                }elseif($workvar[$topkey]['type'] == "image"){

                    $pdf->Image("./resources/image/".$workvar[$topkey]['text'].".png",$workvar[$topkey]['x'],$workvar[$topkey]['y'],$workvar[$topkey]['width']);

              }elseif($workvar[$topkey]['type'] == "gaelogo"){

                  $pdf->Image("./resources/img/gaelogo.png",$workvar[$topkey]['x'],$workvar[$topkey]['y'],$workvar[$topkey]['width']);

              }elseif($workvar[$topkey]['type'] == "gclogo"){

                  $pdf->Image("./resources/pdf/".$gcid.".jpg",$workvar[$topkey]['x'],$workvar[$topkey]['y'],$workvar[$topkey]['width']);

              }elseif($workvar[$topkey]['type'] == "textbox"){

                $pdf->SetXY($workvar[$topkey]['x'], $workvar[$topkey]['y']);

                switch($workvar[$topkey]['fontweight']){
                  case '':
                  $pdf->SetFont('arial', '', $workvar[$topkey]['fontsize']);
                  break;
                  case 'B':
                  $pdf->SetFont('arialb', '', $workvar[$topkey]['fontsize']);
                  break;
                  case 'I':
                  $pdf->SetFont('ariali', '', $workvar[$topkey]['fontsize']);
                  break;
                  case 'BI':
                  $pdf->SetFont('arialbi', '', $workvar[$topkey]['fontsize']);
                  break;
                }


                // print text boxes
                //MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)


                $pdf->MultiCell($workvar[$topkey]['width'],$workvar[$topkey]['height'], $workvar[$topkey]['text'],1, $workvar[$topkey]['align'], 1, 0, '', '', '', '', '', '', $workvar[$topkey]['height'], $workvar[$topkey]['valign'],true);

              }elseif($workvar[$topkey]['type'] == "joinedtext"){

                $pdf->SetXY($workvar[$topkey]['x'], $workvar[$topkey]['y']);

                switch($workvar[$topkey]['fontweight']){
                  case '':
                  $pdf->SetFont('arial', '', $workvar[$topkey]['fontsize']);
                  break;
                  case 'B':
                  $pdf->SetFont('arialb', '', $workvar[$topkey]['fontsize']);
                  break;
                  case 'I':
                  $pdf->SetFont('ariali', '', $workvar[$topkey]['fontsize']);
                  break;
                  case 'BI':
                  $pdf->SetFont('arialbi', '', $workvar[$topkey]['fontsize']);
                  break;
                }


                // print text boxes
                //MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)


              /*  $pdf->MultiCell($workvar[$topkey]['width'],
                $workvar[$topkey]['height'],
                $workvar[$topkey]['text']."test"
                ,1, $workvar[$topkey]['align'], 1, 0, '', '', '', '', '', '',
                $workvar[$topkey]['height'], $workvar[$topkey]['valign'],true);
                */

                function trimHereDoc($txt)
                {
                    return preg_replace('/^\s+|\s+$/m', '', $txt);
                }

                //debug empty spaces
              //  die(var_dump($workvar[$topkey]['text']));

                $pdf->MultiCell($workvar[$topkey]['width'], $workvar[$topkey]['height'], trimHereDoc($workvar[$topkey]['text']), 1, $workvar[$topkey]['align'], 1, 0, '', '', true, $valign=$workvar[$topkey]['valign'],$fitcell=false);

              }

      }

  }
}


  $pdf->Output($_SERVER['DOCUMENT_ROOT'].$f3->get('BASE').$path.$filename.".pdf",$where);

  $f3->reroute('/certificates/success/'.$filename.'.pdf');

}



// Fix PDF output errors with european special chars
function convString($str){
    return mb_convert_encoding($str,'ISO-8859-1',mb_detect_encoding($str));
}

//needed in templates to populate checkboxes
$f3->set('tplCheckTrue',
    function($var) {
        return (isset($var) && $var == true) ? true : false;
    }
);



$f3->route('GET /search/textelements/@string','itemsearch');
$f3->route('POST /search/textelements/@string','itemsearch');
function itemsearch($f3){ // search


    $query = filter_var($f3->get('PARAMS.string'), FILTER_SANITIZE_STRING);
    $db=$f3->get('DB');

        $result = $db->exec("
        SELECT  CONCAT(text,' | ',type) AS result, ident, type, text FROM elements
        WHERE text LIKE '%$query%' OR type LIKE '%$query%'

        ");

        echo json_encode($result);
}

$f3->route('POST /performupload/@certid','CertCustomerLogoUpload');
function CertCustomerLogoUpload($f3){
  // A list of permitted file extensions

$certid=$f3->get('PARAMS.certid');
$allowed = array('png', 'jpg');

if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

	$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);



	if(!in_array(strtolower($extension), $allowed)){
		echo '{"status":"error"}';
		exit;
	}

	if(move_uploaded_file($_FILES['upl']['tmp_name'], 'uploads/'.$f3->get('SESSION.generateid').'.'.$extension)){
    $f3->set('SESSION.generatelogo',$f3->get('SESSION.generateid').'.'.$extension);
		echo '{"status":"success"}';
		exit;
	}
}

echo '{"status":"error"}';


}

$f3->route('POST /performimageupload','CustomImageUpload');
function CustomImageUpload($f3){
  // A list of permitted file extensions

$upload_type=$f3->get('POST.uploadtype');
$upload_name=$f3->get('POST.placeholder_name');

$allowed = array('png', 'jpg');

if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

	$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

//OR strlen(trim($upload_name)<1) OR strlen(trim($upload_type)<1)



	if((!in_array(strtolower($extension), $allowed)) ){
		echo '{"status":"error"}';
		exit;
	}

  if((!empty($upload_type)) && (!empty($upload_name))){
  	if(move_uploaded_file($_FILES['upl']['tmp_name'], 'resources/'.$upload_type.'/'.$upload_name.'.'.$extension)){
      //$f3->set('SESSION.generatelogo',$f3->get('SESSION.generateid').'.'.$extension);
  		echo '{"status":"error"}';
  		exit;
  	}
  }else{
    echo '{"status":"error"}';
  }
}

echo '{"status":"error"}';


}

$f3->route('POST /admin/templateupload/@gclabel/@certid','CertTemplateUpload');
function CertTemplateUpload($f3){
  rerouteNonLoggedin($f3,$f3->get('SESSION.user'));
  // A list of permitted file extensions
if($f3->get('PARAMS.gclabel') == 1){
$certid=$f3->get('PARAMS.certid');
}else{
  $certid="zert";
}
$allowed = array('pdf');

if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

	$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);



	if(!in_array(strtolower($extension), $allowed)){
		echo '{"status":"error"}';
		exit;
	}

  $filename='gc-'.$certid.'.pdf';
  $filepath='resources/pdf/'.$filename;
  $previmg=$filepath.".jpg";

	if(move_uploaded_file($_FILES['upl']['tmp_name'], $filepath)){
    if(file_exists($previmg)){
      unlink($previmg);
    }
		echo '{"status":"success"}';
		exit;
	}
  //see if thumbnail exists - if it does - delete.


}

echo '{"status":"error"}';


}

$f3->route('GET /jsontest','jsontest');
function jsontest($f3){

  $data = array(
    "br-nr" => "12345",
    "br-name" => "Company Name",
    "br-street" => "2F No 24-2 Industry East\nRoad IV, Science Based\nIndustrial Park",
    "br-country" => "Taiwan",
    "br-zip"=>"300",
    "br-city"=>"Hsinchu",
    "br-certdate"=>"2016-01-01",
    "br-valid-until"=>"18.02.1986",
    "br-scope"=>"Manufacturing of Equipment"
  );

  $json = json_encode($data);


$f3->set("mydata",$json);

echo template::instance()->render('jsontestform.htm');

}


function reformatDate($date){
  //reformat date coming from Corporate Database

  return date("Y-m-d", strtotime($date));

}


$f3->route('POST /receivejson','receivejson');
$f3->route('POST /receiveCDBdata','receivejson');
function receivejson($f3){

  $f3->set('html_title','Zertifikatsvorlage wählen');
  $f3->set('navpath','start');
  $f3->set('idtopass','');

  //echo var_dump($f3->get("POST"));
  $data= $f3->get("POST.somedata");
$f3->set('data',$data);
//var_dump(json_decode($data));

$data=json_decode($data);

  //for($f3->get('RemoteKeys'))
  $remotekeys=$f3->get('RemoteKeys');
  $localkeys=$f3->get('LocalKeys');
  $transferdata=array();

  for($i=0;$i<count($localkeys);$i++){
    //  echo $remotekeys[$i];
      $sesskey="SESSION.".$localkeys[$i];
      $transferdata[$localkeys[$i]]=$data->$remotekeys[$i];
      if(($localkeys[$i] == "CertDate") || ($localkeys[$i] == "CertValidUntil")){
        $f3->set($sesskey,reformatDate($data->$remotekeys[$i]));
      }else{
        $f3->set($sesskey,$data->$remotekeys[$i]);
      }


  }

  $f3->set('TransferData',$transferdata);

if(!$f3->get('SESSION.CERTREGNO')){
  $f3->set('SESSION.CERTREGNO',NULL);
  $f3->set('dataset',json_encode($f3->get("POST")));
}



$f3->set('fillform','json_received.htm');

$f3->set('content','new_cert.htm');

echo Template::instance()->render('layout.htm');

}

function getCertTitleByIdent($ident){
  global $f3;
  if($ident == 0){
    $result="ALL";
  }else{
    $cert=new DB\SQL\Mapper($f3->get('DB'),'certificates');
    $cert->load(array('ident=?',$ident));
    if($cert->title){
    $result=$cert->title;
    }else{
      $result="N/A";
    }
  }


  return $result;
}

function unsetTransferData($f3){

  $remotekeys=$f3->get('RemoteKeys');
  $localkeys=$f3->get('LocalKeys');
//  $transferdata=array();

  for($i=0;$i<count($localkeys);$i++){
    //  echo $remotekeys[$i];
      $sesskey="SESSION.".$localkeys[$i];
    //  $transferdata[$localkeys[$i]]=$data->$remotekeys[$i];
      $f3->clear($sesskey);

  }

}

// run application
$f3->run();
