<?php

// include FPDF
require_once('fpdf.php');

// create document
$pdf = new FPDF();

// add font
$pdf->AddFont('Dejavusanscondensed', '', 'Dejavusanscondensed.php');

// add page
$pdf->AddPage();

// set new font
$pdf->SetFont('Dejavusanscondensed', '', 35);

// write text
$pdf->Write(10,'Viel Erfolg mit Ihrer neuen Schriftart Dejavusanscondensed in FPDF!');

// output
$pdf->Output();

?>