<html>

<head>
    <script src="<?php echo $BASE; ?>/js/jquery-2.2.3.min.js"></script>
    <script src="<?php echo $BASE; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $BASE; ?>/lib/bs_datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo $BASE; ?>/js/typeahead.bundle.js"></script>

    <script src="<?php echo $BASE; ?>/js/jquery.knob.js"></script>

    		<!-- jQuery File Upload Dependencies -->
    		<script src="<?php echo $BASE; ?>/js/jquery.ui.widget.js"></script>
    		<script src="<?php echo $BASE; ?>/js/jquery.iframe-transport.js"></script>
    		<script src="<?php echo $BASE; ?>/js/jquery.fileupload.js"></script>

    <link rel="stylesheet" href="<?php echo $BASE; ?>/lib/bs_datepicker/css/bootstrap-datepicker3.min.css" media="all" />
    <link rel="stylesheet" href="<?php echo $BASE; ?>/ui/css/bootstrap.min.css" media="all" />
    <link rel="stylesheet" href="<?php echo $BASE; ?>/ui/css/dashboard.css" media="all" />
    <link rel="shortcut icon" href="<?php echo $BASE; ?>/ui/images/favicon.ico">
    <link rel="icon" type="image/x-icon" href="<?php echo $BASE; ?>/ui/images/favicon.ico" />
    <link rel="icon" type="image/png" href="<?php echo $BASE; ?>/ui/images/favicon.png" sizes="32x32">
    <title><?php echo $APPNAME; ?></title>
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img class="navbar-brand" src="<?php echo $BASE; ?>/ui/images/cfs_certapp_wht.png" class="img-responsive" alt="Responsive image">
                


            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <?php foreach (($menu?:array()) as $navkey=>$navval): ?>
                        <?php if ($navpath == $navval['path']): ?>
                            
                                <li class="active"><a href="<?php echo $BASE; ?>/<?php echo $navval['path']; ?>"><span class="glyphicon glyphicon-<?php echo $navval['icon']; ?>"></span> <?php echo $navval['label']; ?></a></li>
                            
                            <?php else: ?>
                                <li><a href="<?php echo $BASE; ?>/<?php echo $navval['path']; ?>"><span class="glyphicon glyphicon-<?php echo $navval['icon']; ?>"></span> <?php echo $navval['label']; ?></a></li>
                            
                        <?php endif; ?>

                    <?php endforeach; ?>
                    <?php if (isset($SESSION['user'])): ?>
                        

                            <?php foreach (($admin_menu?:array()) as $navkey=>$navval): ?>
                                <?php if ($navpath == $navval['path']): ?>
                                    
                                        <li class="active"><a href="<?php echo $BASE; ?>/<?php echo $navval['path']; ?>"><span class="glyphicon glyphicon-<?php echo $navval['icon']; ?>"></span> <?php echo $navval['label']; ?></a></li>
                                    
                                    <?php else: ?>
                                        <li><a href="<?php echo $BASE; ?>/<?php echo $navval['path']; ?>"><span class="glyphicon glyphicon-<?php echo $navval['icon']; ?>"></span> <?php echo $navval['label']; ?></a></li>
                                    
                                <?php endif; ?>

                            <?php endforeach; ?>

                            <li class="logout"><a href="<?php echo $BASE; ?>/logout"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
                        
                        <?php else: ?>

                            <li><a href="<?php echo $BASE; ?>/login"><span class="glyphicon glyphicon-cog"></span> Admin</a></li>
                        
                    <?php endif; ?>




                </ul>

            </div>
        </div>
    </nav>





    <div class="col-sm-12 col-md-12  main">

        <div class="maincontent">
            <?php echo $this->render($content,$this->mime,get_defined_vars()); ?>
        </div>

    </div>
    </div>
    </div>





</body>

</html>
