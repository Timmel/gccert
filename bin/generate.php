<?php
$f3 = require_once('/lib/base.php');
$f3->set('DEBUG', 3);
$f3->config('config.ini');

require_once('/lib/fpdf/fpdf.php');
require_once('/lib/fpdf/textbox.php');
require_once('/lib/fpdi/fpdi.php');
require_once('/lib/qr.php');


printCertificate($f3);

//$f3->route('POST /certificates/generate/@id','printCertificate()');
//$f3->route('POST /certificates/generate/','generateCert');



function printCertificate($f3,$input="",$type="GC",$lang="de"){

    $width=300;
    $height=300;
    $az=491767;

    $_tpl["tpl_img"]="./resources/img/gc-zert.jpg";
    $_tpl["tpl_pdf"]="./resources/pdf/gc-zert.pdf";
    $_tpl["url"]="https://www.mydqs.com/kunden/kundendatenbank.html?aoemydqs%5Bcompany_no%5D={$az}&aoemydqs%5Baction%5D=singleView";


    // Instanciation of inherited class
   // initiate FPDI
    $pdf = new FPDI();


    // add a page
    $pdf->AddPage();
    // set the source file
    $pdf->setSourceFile($_tpl["tpl_pdf"]);
    // import page 1
    $tplIdx = $pdf->importPage(1);
    // use the imported page and place it at position 10,10 with a width of 100 mm
    $pdf->useTemplate($tplIdx, 0, 0, 210);




    $pdf->SetTitle('CERTIFICATE',1);

    $pdf->SetMargins(50,20,15);
    $pdf->SetAutoPageBreak(0, 0);

    $pdf->SetTextColor(0, 0, 0);

    // HEADING
    $pdf->SetFont('Arial','B');
    $pdf->SetXY(50, 30);
    $pdf->SetFontSize(40);
    $pdf->Write(0, 'CERTIFICATE');


    // to certify that
    $pdf->SetFont('Arial');
    $pdf->SetXY(50, 55);
    $pdf->SetFontSize(12);
    $pdf->Write(0, 'This is to certify that');


    // COMPANY BLOCK
    $pdf->SetFont('Arial','B');
    $pdf->SetFontSize(20);
    $pdf->SetXY(50, 75);
    $pdf->Write(0, 'Company');

    $pdf->SetFont('Arial');
    $pdf->SetFontSize(12);

    $pdf->SetXY(50, 85);
    $pdf->Write(0, 'Address Line 1');
    $pdf->SetXY(50, 90);
    $pdf->Write(0, 'Address Line 2');
    $pdf->SetXY(50, 95);
    $pdf->Write(0, 'Country');

    // Text

    $certtext="have engaged DQS to provide independent assurance over its sustainability report, compiled according to the GRI G4 Guidelines. The engagement took place from xx.xx.201x to xx.x.201x.

The assurance engagement was performed in accordance with a Type x assurance of the AA1000 Assurance Standard (AA1000AS 2008). The findings can be read in the Assurance Statement.";

    $pdf->SetFont('Arial');
    $pdf->SetFontSize(11);
    $pdf->SetXY(50, 110);


   // $pdf->drawTextBox($certtext, 150, 50, 'J', 'T',0);
    $pdf->MultiCell(0,5,$certtext,0,"J");


    //lower part
    $pdf->SetFont('Arial');
    $pdf->SetFontSize(12);
    $pdf->SetXY(50, 165);
    $pdf->Write(0, 'The organization is authorized to bear the following GC-Mark:');

    //Cert Mark
    $pdf->SetFont('Arial','B');
    $pdf->SetFontSize(20);
    $pdf->SetXY(50, 175);
    $pdf->Write(0, 'Verified GRI Report');


    //Cert Info Block
    $pdf->SetFont('Arial');
    $pdf->SetFontSize(12);

    $pdf->SetXY(50, 190);
    $pdf->Write(0, 'Certificate registration no.');

    $pdf->SetXY(50, 198);
    $pdf->Write(0, 'Date of certification');

    $pdf->SetXY(50, 206);
    $pdf->Write(0, 'Valid until');


    // specific data
    $pdf->SetXY(110, 190);
    $pdf->Write(0, '1233454 GC');

    $pdf->SetXY(110, 198);
    $pdf->Write(0, '2016/12/12');

    $pdf->SetXY(110, 206);
    $pdf->Write(0, '2017/12/11');


    // SIGNATURE
    $pdf->Image("./resources/signatures/sign_sad.png",50,225,30);

    $pdf->SetFont('Arial','B');
    $pdf->SetFontSize(12);
    $pdf->SetXY(50, 225);
    $pdf->Write(0, 'DQS CFS GmbH');

    $pdf->SetFont('Arial');
    $pdf->SetFontSize(10);
    $pdf->SetXY(50, 250);
    $pdf->Write(0, 'Dr. Sied  Sadek');
    $pdf->SetXY(50, 255);
    $pdf->Write(0, 'Managing Director');



    //IMAGES
    $pdf->Image(generate_qr($_tpl["url"]),180,262,20);


    // FOOTER
    $pdf->SetFont('Arial');
    $pdf->SetFontSize(10);

    $pdf->SetXY(50, 275);
    $pdf->Write(0, 'Certification Body:');
    $pdf->SetXY(50, 280);
    $pdf->Write(0, convString('DQS CFS GmbH, August-Schanz-Straße 21, 60433 Frankfurt am Main'));




    $pdf->Output();

}

function convString($str){
    return mb_convert_encoding($str,'Windows-1252');
}


?>
